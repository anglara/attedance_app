<?php
//UserController: Insert , update , delete and view records from user table
class UserController 
{
	// User Manage (insert record)
	// 	Name [ required ]
	// 	Email [ unique ]
	// 	Phone no
	// 	Age
	// 	Gender

	function insertUser()
	{
		if($_SERVER['REQUEST_METHOD'] == "POST" )
		{
			$nameUser = $_POST['nameUser'];
			$emailUser = $_POST['emailUser'];
			$contactUser = $_POST['contactUser'];
			$ageUser = $_POST['ageUser'];
			$gen="";
			if(empty($_POST['gender']))
			{
				$gen ="*Please select gender";
			}		
			if($nameUser != " " && $emailUser != "" && $contactUser != ""  && $gen == "")
			{
				$result = "insert into user (name,email,phone,age,gen) values ('".$nameUser."','".$emailUser."','".$contactUser."','".$ageUser."','".$_POST['gender']."')";
				if(mysqli_query($GLOBALS['con'],$result))
				{
					header("location:viewUsers.php");
				}
				else
				{
					echo mysqli_error($GLOBALS['con']);
				}
			}			
		}
	}
	//View All Users
	public function getUsers()
	{
		global $sql;
		$sql = "select * from user";
		global $record;
		$record = $GLOBALS['con']->query($sql);
		return $record;
	}
	//View single User
	public function showUser($id)
	{
		$sql = "select * from user where id='".$id."'";
		global $record;
		$record = $GLOBALS['con']->query($sql);
		return $record;
	}
	//Edit User Details 
	public function editUser($id)
	{
		
		if(isset($_POST['submit']))
		{
			$nameUser = $_POST['nameUser'];
			$emailUser = $_POST['emailUser'];
			$contactUser = $_POST['contactUser'];
			$ageUser = $_POST['ageUser'];
					
			$result ="update user set name='".$nameUser."',email='".$emailUser."',phone='".$contactUser."',age ='".$ageUser."',gen='".$_POST['genderUser']."' where id='".$id."' ";
				
			if(mysqli_query($GLOBALS['con'],$result))
			{
				header("location: viewUsers.php");
			}
		}
	}
	//Delete single record
	public function destroyUser($id)
	{
		$result=("delete from user where id='".$id."' "); 
        if(mysqli_query($GLOBALS['con'],$result))
		{
			header("location: viewUsers.php");
		} 
    }
}

?>