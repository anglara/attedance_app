<?php 
// include connection file(for database connection) and controller files(for CRUD oprations) 

include "C:\wamp\www\anglara\Attendance_Project\model\connection.php";
include "C:\wamp\www\anglara\Attendance_Project\controller\AttendanceController.php";

//get id from server
global $id;
$id = $_GET['id'];

//variable for validation
$fromdate = $todate = $leaveday = $userid =" " ;

//validation

if($_SERVER['REQUEST_METHOD'] == "POST")
{
	//validation 

	if(empty($_POST['todate']))
	{
		$todate ="* Ending date is required";
	}
	if(empty($_POST['fromdate']))
	{
		$fromdate ="* Starting date is required";
	}
	if(empty($_POST['leaveday']))
	{
		$leaveday ="* leaveday is required";
	}
	if(empty($_POST['userid']))
	{
		$Userid ="* Please select Userid";
	}

	$obj =new AttendanceController();
	$obj->showAttendance($id);	
	$obj->editAttendance($id);	
}

?> 	 	 
<!-- Edit leave view page -->
<!DOCTYPE html>
<html>
<head>
	<title>Add User</title>
	<!--bootstrap CSS only -->
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
	<!-- for icon -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
	<div class="container-fluid bg-secondary mt-0 pt-0">
		<div class="container bg-secondary">
			<div class="row py-5">
				<div class=" col-6">
					<a href="#" class=" text-decoration-none text-white fw-bold fs-5">logo</a>
				</div>
				<div class=" col-6" align="right">
					<a href="..\attendance\viewAttendance.php" class=" m-2 p-2 text-decoration-none bg-white text-black fw-bold">Back</a>
				</div>
			</div>
		</div>
	</div>
	<div class="container bg-secondary">
		<div class="row bg-white"><div class="col-lg-3"></div>
				<div class="col-lg-6" align="center">
					<form action="#" method="POST">
						<table class="table table-striped">
						<thead class="thead-dark">
							<tr>
								<th colspan="2"> <h1>Edit User</h1></th>
							</tr>
						</thead>
						<!-- return single record userattendance table for update record -->
							<?php 
								$sql = "select * from userattendance where id='".$id."'";
								$record = $GLOBALS['con']->query($sql);

							if($record->num_rows > 0){
								while($r = mysqli_fetch_array($record)){

							?>
						<tbody>
							<tr>
								<th>
									Id
								</th>
								<td><label class="form-control" ><?php echo $r['id']; ?></label>
								</td>
							</tr>
							<tr>
								<th>
									Starting Date
								</th>
								<td>
									<input class="form-control" type="date" name="fromdate" value="<?php echo $r['fromdate']; ?>"><span class="text-danger"> <?php echo $fromdate; ?></span>
								</td>
							</tr>
							<tr>
								<th>Endding Date</th>
								<td><input class="form-control" type="date" name="todate" value="<?php echo $r['todate']; ?>"><span class="text-danger"> <?php echo $todate ;?></span>
								</td>
							</tr>
							<tr>
								<th>Leave</th>
								<td>
									<textarea name="leaveday" class="form-control">
										<?php echo $r['leaveday'];?></textarea>
									<span class="text-danger"> <?php echo $leaveday; ?></span>				
								</td>
							</tr>
							<tr>
								<th>User ID</th>
								<td>
									<select name="userid" class="form-control">
										<option><?php echo $r['userid']; ?></option>

									<!-- get data from user table for user id -->
									
									<?php
										$sql="select *from user";									
										$record = $GLOBALS['con']->query($sql);
										
										if($record->num_rows > 0)
										{
											while($r1 = mysqli_fetch_array($record))
											{
											
									?>
										<option><?php echo $r1['id']; ?></option>
									<?php 
											}
										}
									?>
									</select>
									<span class="text-danger"> <?php echo $userid;?></span>
								</td>
							</tr>
						<?php 
							}
						}
						?>
							<tr>
								<td colspan="2" align="right">
									<input type="submit" class="bg-secondary border-0 text-white p-2" name="submit" value="Update Details">
									
								</td>
							</tr>
						</tbody>
					</table>
					</form>
				</div>
		</div>
	</div>
</body>
</html>