<?php 

// include connection file(for database connection) and controller files(for CRUD oprations) 

include "C:\wamp\www\anglara\Attendance_Project\model\connection.php";
include "C:\wamp\www\anglara\Attendance_Project\controller\AttendanceController.php";
	if(isset($_POST['submit']))
	{
		$id=$_GET['id'];
		//create instance of attendance controller
		$delete = new AttendanceController();
		$delete->destroyAttendance($id);
	}								
?>
<!DOCTYPE html>
<html>
<head>
	<title>Add User</title>
	<!-- CSS only -->
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
<body>
	<div class="container-fluid bg-secondary mt-0 pt-0">
		<div class="container bg-secondary">
			<div class="row py-5">
				<div class=" col-6">
					<a href="#" class=" text-decoration-none text-white fw-bold fs-5">logo</a>
				</div>
				<div class=" col-6" align="right">
					<form class="form-inline">
					    <input class="" type="search" placeholder="Search" aria-label="Search">
					    <button class="btn bg-white text-black" type="submit">Search</button>
					  </form>
				</div>
			</div>
		</div>
	</div>
	<!--  Navbar  -->

	<nav class="navbar navbar-expand-lg navbar-light bg-light">
	  <a class="navbar-brand px-3" href="#">Attendance App</a>
	  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
	    <span class="navbar-toggler-icon"></span>
	  </button>
	  <div class="collapse navbar-collapse" id="navbarNav">
	    <ul class="navbar-nav">
	      <li class="nav-item active">
	        <a class="nav-link" href="..\user\viewUsers.php">User </a>
	      </li>
	      <li class="nav-item">
	        <a class="nav-link" href="..\attendance\viewAttendance.php">Attendance</a>
	      </li>
	    </ul>
	  </div>
	</nav>
	<div class="container bg-secondary">
		<div class="row bg-white">
			<div class="col-12">
				<table class="table table-striped">
					<thead class="thead-dark">
						<tr>
							<td colspan="5">
								<h2>User Information</h2>
							</td>
							<td>
								<a href="addAttendance.php" class=" my-2 p-2 text-decoration-none bg-secondary text-white fw-bold">Add Attendance</a>
							</td>
						</tr>
						<tr>
							<th >#</th>
							<th >From Date</th>
							<th >To Date</th>
							<th >Leave</th>
							<th >User Id</th>
							<th >Action</th>
						</tr>
					</thead>
			<!-- view all records from leave table -->
				<?php 
					$view = new AttendanceController();
					$view->getAttendance();
					if($record->num_rows > 0){
						while($r = mysqli_fetch_array($record)){
				?>
				<tbody>
					<td><?php echo $r['id'] ?></td>
					<td><?php echo $r['fromdate'] ?></td>
					<td><?php echo $r['todate'] ?></td>
					<td><?php echo $r['leaveday'] ?></td>
					<td><?php echo $r['userid'] ?></td>
					<td>
						<form action="viewAttendance.php?id=<?php echo $r['id']; ?>" method="post">
							<a href="editAttendance.php?id=<?php echo $r['id']; ?>" class="fa fa-pencil text-decoration-none bg-dark text-white rounded-3 p-2"></a>
							<a href="showAttendance.php?id=<?php echo $r['id']; ?>" class="fa fa-eye text-decoration-none bg-dark text-white rounded-2 p-2"></a>
							<button class="fa fa-trash text-decoration-none bg-dark text-white rounded-3 p-2" value="Delete" type="submit" name="submit" onclick="return confirm('are you sure?')"></button>
						</form> 
					</td>
					</tbody>
					<?php
							}
						}
					?>
				</table>
			</div>
		</div>
	</div>
</body>
</html>