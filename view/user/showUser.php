<?php 
// include connection file(for database connection) and controller files(for CRUD oprations) 

include "C:\wamp\www\anglara\Attendance_Project\model\connection.php";
include "C:\wamp\www\anglara\Attendance_Project\controller\UserController.php";
?>	
<!DOCTYPE html>
<html>
<head>
	<title>Add User</title>
	<!-- bootstrap CSS only -->
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">

	<!-- Fafa icon -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
	<div class="container-fluid bg-secondary mt-0 pt-0">
		<div class="container bg-secondary">
			<div class="row py-5">
				<div class=" col-6">
					<a href="#" class=" text-decoration-none text-white fw-bold fs-5">logo</a>
				</div>
				<div class=" col-6" align="right">
					<a href="viewUsers.php" class=" m-2 p-2 text-decoration-none bg-white text-black fw-bold">Back</a>
				</div>
			</div>
		</div>
	</div>	
	<div class="container bg-secondary">
		<div class="row bg-white"><div class="col-lg-3"></div>
				<div class="col-lg-6" align="center">
					<form action="#" method="POST">
						<table class="table table-striped">
						<thead class="thead-dark">
							<tr>
								<th colspan="2"> <h1>User Information</h1></th>
							</tr>
						</thead>
						<?php
							$id = $_GET['id'];
							$obj=new UserController();
							$obj->showUser($id);
							if($record->num_rows > 0){
								while($r = mysqli_fetch_array($record)){

						?>
						<tbody>
							<tr>
								<th>
									Id
								</th>
								<td><?php echo $r['id']; ?>
								</td>
							</tr>
							<tr>
								<th>
									Name
								</th>
								<td><?php echo $r['name']; ?>
								</td>
							</tr>
							<tr>
								<th>Email</th>
								<td><?php echo $r['email']; ?></td>
							</tr>
							<tr>
								<th>Phone No</th>
								<td><?php echo $r['phone']; ?>
								</td>
							</tr>
							<tr>
								<th>Age</th>
								<td><?php echo $r['age']; ?>
								</td>
							</tr>
							<tr>
								<th>Gender</th>
								<td><?php echo $r['gen']; ?>
								</td>
							</tr><?php } }?>
						</tbody>
					</table>
					</form>
				</div>
		</div>
</body>
</html>