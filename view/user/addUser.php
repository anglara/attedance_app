<?php 
// include connection file(for database connection) and controller files(for CRUD oprations) 

	include "C:\wamp\www\anglara\Attendance_Project\model\connection.php";
	include "C:\wamp\www\anglara\Attendance_Project\controller\UserController.php";

	$name = $email = $phone = $age = $gen = $sucess = " " ;
	$nameUser = $emailUser = $contactUser = $ageUser = $genderUser = " ";

if($_SERVER['REQUEST_METHOD'] == "POST")
{
	//validation 

	if(empty($_POST['nameUser']))
	{
		$name ="* Name is required";
	}
	if(empty($_POST['emailUser']))
	{
		$email ="* Email is required";
	}
	if($_POST['emailUser'] != " ")
	{
		$em=$_POST['emailUser'] ;
		$result="select * from user where email = '".$em."'";
		if($data = mysqli_query($con , $result)){
			$records = mysqli_num_rows($data);
			if($records > 0)
			{
				$email="* Email is already exits";
			}
		}
	}
	if(empty($_POST['contactUser']))
	{
		$phone ="* Phone number is required";
	}
	if(preg_match('/^[0-9]{10}+$/', $_POST['contactUser'])) {
		$phone="";
	}
	else
	{ 
		$phone="* Invalid Phone Number";
	}
	if($_POST['ageUser'] <= 18 ){
		$age="* Age only enter grater then than 17";
	}
	if($_POST['ageUser'] == "" ){
		$age="* Age is required";
	}
	if(empty($_POST['gender']))
	{
		$gen ="* Please select gender";
	}	
}

$obj =new UserController();
$obj->insertUser();	

?>
<!DOCTYPE html>
<html>
<head>
	<title>Add User</title>
	<!-- CSS only -->
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
	<div class="container-fluid bg-secondary mt-0 pt-0">
		<div class="container bg-secondary">
			<div class="row py-5">
				<div class=" col-6">
					<a href="#" class=" text-decoration-none text-white fw-bold fs-5">logo</a>
				</div>
				<div class=" col-6" align="right">
					<a href="viewUsers.php" class=" m-2 p-2 text-decoration-none bg-white text-black fw-bold">Back</a>
				</div>
			</div>
		</div>
	</div>
	<div class="container bg-secondary">
		<div class="row bg-white"><div class="col-lg-3"></div>
				<div class="col-lg-6" align="center">
					<form action="#" method="POST">
						<table class="table table-striped">
						<thead class="thead-dark">
							<tr>
								<th colspan="2"> <h1>Add User</h1></th>
							</tr>
						</thead>

						<tbody>
							<tr>
								<th>
									Name
								</th>
								<td>
									<input class="form-control" id="nm" type="text" name="nameUser" placeholder="Enter Your Fullname">
									<span class="text-danger" id="err"><?php echo $name; ?></span>
								</td>
							</tr>
							<tr>
								<th>Email</th>
								<td><input class="form-control" id="em" type="email" name="emailUser" placeholder="Enter Your EmailID">
									<span class="text-danger" id="err"> <?php echo $email; ?></span>
								</td>
							</tr>
							<tr>
								<th>Phone No</th>
								<td>
									<input class="form-control" id="phn" type="number" name="contactUser" placeholder="Enter Your Contact Info">	
									<span class="text-danger" id="err"> <?php echo $phone; ?></span>

								</td>
							</tr>
							<tr>
								<th>Age</th>
								<td>
									<input class="form-control" id="age" type="number" min="18" name="ageUser" placeholder="Select Your Age">
									<span class="text-danger" id="err"> <?php echo $age; ?></span>

								</td>
							</tr>
							<tr>
								<th>Gender</th>
								<td>
									<input type="radio" name="gender" value="Female"> Female 
									<input type="radio" name="gender" value="Male"> Male 
									<input type="radio" name="gender" value="Other"> Other
									<span class="text-danger" id="err"><?php echo $gen; ?></span>

								</td>
							</tr>
							<tr>
								<td colspan="2" align="right">
									<input type="submit" class="bg-secondary border-0 text-white p-2" name="submit" value="Add Details">
									<input type="button" class="bg-secondary border-0 text-white p-2" name="clear" onclick="reset();" value="Clear">
									
								</td>
							</tr>
						</tbody>
					</table>
					</form>
				</div>
		</div>
	</div>
</body>
</html>