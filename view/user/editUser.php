<?php 
// include connection file(for database connection) and controller files(for CRUD oprations) 

include "C:\wamp\www\anglara\Attendance_Project\model\connection.php";
include "C:\wamp\www\anglara\Attendance_Project\controller\UserController.php";
//get id from server
global $id;
$id = $_GET['id'];

$name = $email = $phone = $age = $gen = " ";
	// validation
if($_SERVER['REQUEST_METHOD'] == "POST")
{	
	
	if(!(preg_match('/^[0-9]{10}+$/', $_POST['contactUser']))) {
		$phone="* Invalid Phone Number";
	}
	else if(empty($_POST['nameUser']))
	{
		$name ="* Name is required";
	}
	else if(empty($_POST['emailUser']))
	{
		$email ="* Email is required";
	}
	else if(empty($_POST['contactUser']))
			{
		$phone ="* Phone number is required";
	}
	else if($_POST['ageUser'] <= 18 ){
		$age="* Invalid Age (only valid up to 18)";
	}
	else if($_POST['ageUser'] == "" ){
		$age="* Age is required";
	}
	else{

		$obj= new UserController();
		$obj->editUser($id);
	
	}
}
?> 	 	 
<!-- edit user record -->
<!DOCTYPE html>
<html>
<head>
	<title>Add User</title>
	<!-- CSS only -->
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
	<div class="container-fluid bg-secondary mt-0 pt-0">
		<div class="container bg-secondary">
			<div class="row py-5">
				<div class=" col-6">
					<a href="#" class=" text-decoration-none text-white fw-bold fs-5">logo</a>
				</div>
				<div class=" col-6" align="right">
					<a href="viewUsers.php" class=" m-2 p-2 text-decoration-none bg-white text-black fw-bold">Back</a>
				</div>
			</div>
		</div>
	</div>
	<div class="container bg-secondary">
		<div class="row bg-white"><div class="col-lg-3"></div>
				<div class="col-lg-6" align="center">
					<form action="#" method="POST">
						<table class="table table-striped">
						<thead class="thead-dark">
							<tr>
								<th colspan="2"> <h1>Edit User</h1></th>
							</tr>
						</thead>
							<?php 

								$obj=new UserController();
								$obj->showUser($id);						
								if($record->num_rows > 0)
								{
									while($r = mysqli_fetch_array($record))
									{
							?>
						<tbody>
							
							<tr>
								<th>
									Id
								</th>
								<td><label class="form-control" ><?php echo $r['id']; ?></label>
								</td>
							</tr>
							<tr>
								<th>
									Name
								</th>
								<td>
									<input class="form-control" type="text" name="nameUser" value="<?php echo $r['name']; ?>"><span class="text-danger"> <?php echo $name; ?></span>
								</td>
							</tr>
							<tr>
								<th>Email</th>
								<td><input class="form-control" type="email" name="emailUser" value="<?php echo $r['email']; ?>"><span class="text-danger"> <?php echo $email ;?></span>
								</td>
							</tr>
							<tr>
								<th>Phone No</th>
								<td>
									<input class="form-control" type="number" name="contactUser" value="<?php echo $r['phone']; ?>"><span class="text-danger"> <?php echo $phone; ?></span>				
								</td>
							</tr>
							<tr>
								<th>Age</th>
								<td>
									<input class="form-control" type="number" name="ageUser" value="<?php echo $r['age']; ?>"><span class="text-danger"> <?php echo $age ;?></span>
								</td>
							</tr>
							<tr>
								<th>Gender</th>
								<td>
									<input type="radio" name="genderUser"<?php if($r['gen'] == "Female"){ echo "checked";} ?> value="Female"> Female 
									<input type="radio" name="genderUser"<?php if($r['gen'] == "Male"){ echo "checked";} ?> value="Male"> Male 
									<input type="radio" name="genderUser"<?php if($r['gen'] == "Other"){ echo "checked";} ?> value="Other"> Other
									<span class="text-danger"><?php echo $gen; ?></span>
								</td>
							</tr>
							<?php 
								}}
							?>
							<tr>
								<td colspan="2" align="right">
									<input type="submit" class="bg-secondary border-0 text-white p-2" name="submit" value="Update Details">
									
								</td>
							</tr>
						</tbody>
					</table>
				</form>
			</div>
		</div>
	</div>
</body>
</html>